
export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      // { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://files.vjpromo.com/bannerflow/lp-core/css/bootstrap.min.css'},
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/icon?family=Material+Icons'},
      { rel: 'stylesheet', href: 'https://files.vjpromo.com/bannerflow/lp-core/css/animate.min.css'},
      { rel: 'stylesheet', href: 'https://files.vjpromo.com/bannerflow/lp-core/css/hover-min.css'},
      { rel: 'stylesheet', href: 'https://files.vjpromo.com/bannerflow/lp2/css/core.css'},
      { rel: 'stylesheet', href: 'https://files.vjpromo.com/bannerflow/lp2/brand_icjp/css/brand.css'},
      { rel: 'stylesheet', href: 'https://files.vjpromo.com/bannerflow/lp2/css/responsive.css'}
    ],
    script: [
      { src: 'https://files.vjpromo.com/bannerflow/lp-core/js/jquery-1.11.3.min.js'},
      { defer:'', src: 'https://files.vjpromo.com/bannerflow/lp2/brand_icjp/js/before.js'},

      { defer:'', src: 'https://files.vjpromo.com/bannerflow/lp2/brand_icjp/js/after.js'},
      { type: 'text/javascript', src: 'https://files.vjpromo.com/bannerflow/lp-core/brand_icjp/js/form.js'},
      { type: 'text/javascript', src: 'https://files.vjpromo.com/bannerflow/lp-core/brand_icjp/js/footer.js'},

      { defer:'', src: 'https://files.vjpromo.com/bannerflow/lp-core/js/popper.min.js'},
      { defer:'', src: 'https://files.vjpromo.com/bannerflow/lp-core/js/bootstrap.min.js'},
      { defer:'', src: 'https://files.vjpromo.com/bannerflow/lp-core/js/jquery.flexverticalcenter.js'}
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
